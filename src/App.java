import com.devcamp.JBR1s10.Circle;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3);

        System.out.println(circle1);
        System.out.println(circle2);
    }
}